import 'dart:io';

List tokenizing(String exprs) {
  List symbols = ["+", "-", "*", "/", "%", "^", "(", ")"];
  List numbers = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];

  // Remove whitespace on front and back of string
  exprs = exprs.trim();

  // Remove duplicate whitespace
  final _whitespaceRE = RegExp(r"\s+");
  exprs = exprs.split(_whitespaceRE).join(" ");

  // Split adjacent operators
  symbols.forEach((i) {
    symbols.forEach((j) {
      exprs = exprs.replaceAll("$i$j", "$i $j");
    });
  });

  // Split adjacent operator and number
  numbers.forEach((i) {
    symbols.forEach((j) {
      // Split adjacent operator and number in case operator is on front
      exprs = exprs.replaceAll("$j$i", "$j $i");
      // Split adjacent operator and number in case number is on front
      exprs = exprs.replaceAll("$i$j", "$i $j");
    });
  });
  exprs = exprs.replaceAll(" ", "|");
  // Split adjacent operator and negative number
  numbers.forEach((i) {
    symbols.forEach((j) {
      if (j != ")") {
        exprs = exprs.replaceAll("$j|-|$i", "$j|- $i");
      }
    });
  });

  // Deal with exponent
  numbers.forEach((i) {
    numbers.forEach((j) {
      exprs = exprs.replaceAll("$i|^|$j", "$i ^ $j");
    });
  });

  exprs = exprs.replaceAll("||", "|");

  // Deal with negative symbol on exponent
  numbers.forEach((i) {
    numbers.forEach((j) {
      exprs = exprs.replaceAll("-|$i ^ $j", "- $i ^ $j");
      exprs = exprs.replaceAll("- |$i ^ $j", "- $i ^ $j");
      exprs = exprs.replaceAll("$i|^|-$j", "$i ^ - $j");
      exprs = exprs.replaceAll("$i|^|- $j", "$i ^ - $j");
      exprs = exprs.replaceAll("-|$i|^|-$j", "- $i ^ - $j");
    });
  });

  exprs = exprs.replaceAll("||", "|");

  // Deal with first negative number
  numbers.forEach((i) {
    if (exprs.substring(0, 3) == "-|$i") {
      exprs = exprs.replaceRange(0, 3, "- $i");
    }
  });

  exprs = exprs.replaceAll("||", "|");

  // Collect tokenizing words in list
  var split = exprs.split("|");

  //print(exprs);
  return split;
}

int checkPrecedenceLevel(String operator) {
  if (operator == "(" || operator == ")") {
    return 4;
  }
  if (operator == "^") {
    return 3;
  }
  if (operator == "*" || operator == "/") {
    return 2;
  }
  if (operator == "%") {
    return 3;
  }
  if (operator == "+" || operator == "-") {
    return 1;
  }
  return 0;
}

bool isInteger(String str) {
  if (str == null) {
    return false;
  }
  return double.tryParse(str) != null;
}

List toPostFix(List infix) {
  List operators = [];
  List postfix = [];
  infix = tokenizingExponent(infix);
  //infix = tokenizingNegative(infix);
  print("Your infix $infix");
  infix.forEach((i) {
    if (isInteger(i) == true) {
      //print("1.1 "+i[j]+" is an integer and aded to postfix");

      // Add the token to the end of postfix
      postfix.add(i);

      //print("1.2 Postfix after add integer "+i[j]+" $postfix");
    } else {
      // For each token in the infix expression
      for (int j = 0; j < i.length; j++) {
        if (i[j] != " ") {
          // If the token is an integer Then
          if (isInteger(i[j]) == true) {
            //print("1.1 "+i[j]+" is an integer and aded to postfix");

            // Add the token to the end of postfix
            postfix.add(i[j]);

            //print("1.2 Postfix after add integer "+i[j]+" $postfix");
          }

          // If the token is an operator Then
          if (isInteger(i[j]) == false && i[j] != "(" && i[j] != ")") {
            String str = i;
            if (str.contains("- ")) {
              postfix.add(i);
              break;
            }
            //print("2.1 "+i[j]+" is an operator and not parenthesis");

            // While operators is not empty and
            //    the last item in operators is not an open parenthesis and
            //    precedence(token) < precedence(last item in operator) dp
            while (operators.isEmpty == false &&
                operators.last != "(" &&
                checkPrecedenceLevel(i[j]) <=
                    checkPrecedenceLevel(operators.last)) {
              //print("2.2 "+i[j]+" is not open parenthesis and precedence level is less then "+operators.last);
              //print("2.3 Postfix before add last element of operators : $postfix");
              postfix.add(operators.last);
              //print("2.4 Postfix after add last element of operators : $postfix");
              //print("2.5 Operators before remove last element : $operators");
              operators.removeLast();
              //print("2.6 Operators after remove last element : $operators");
            }
            // Add tpken to the end of operators
            //print("Working?");
            operators.add(i[j]);
            //print("2.7 Operators after add token "+i[j]+" : $operators");
          }

          // If the token is an open parenthesis Then
          if (i[j] == "(") {
            //print("3.1 "+i[j]+" is an open parenthesis");
            // Add the token to the end of operators
            //print("3.2 Operators before add token i[j] : $operators");
            operators.add(i[j]);
            //print("3.3 Operators after add token i[j] : $operators");
          }

          // If the token is a close parenthesis Then
          if (i[j] == ")") {
            //print("4.1 "+i[j]+" is a close parenthesis");
            //print(operators);
            // While the last item in operators is not an open parenthesis do
            while (operators.last != "(") {
              //print("4.2 Now last element in operators is "+operators.last+" which is not open parenthesis");
              // Remove the last item from operators and add it to postfix
              //print("4.3 Postfix before add last element of operators : $postfix");
              postfix.add(operators.last);
              //print("4.4 Postfix after add last element of operators : $postfix");
              //print("4.5 Operators before remove last element : $operators");
              operators.removeLast();
              //print("4.6Operators after remove last element : $operators");
            }
            //print("4.7 Now last element in operators is "+operators.last+" which is open parenthesis");
            //Remove the open parenthesis from operators
            //print("4.8 Operators before remove last element : $operators");
            operators.removeLast();
            //print("4.9 Operators after remove last element : $operators");
          }
        }
      }
    }
  });
  // While operators is not the empty list do
  while (operators.isEmpty == false) {
    //print("5.1 operators is not empty");
    // Remove the last item from operators and add it to postfix
    //print("5.2 Postfix before add last element of operators : $postfix");
    postfix.add(operators.last);
    //print("5.3 Postfix after add last element of operators : $postfix");
    //print("5.4 Operators before remove last element : $operators");
    operators.removeLast();
    //print("5.5 Operators after remove last element : $operators");
  }

  return postfix;
}

List tokenizingExponent(List lst) {
  List tokenized = [];
  List tempList = [];
  //print("lst : $lst");
  for (int h = 0; h < lst.length; h++) {
    String temp = lst[h];
    //print("Current string : $temp");
    if (temp.contains(' ^ ')) {
      temp = temp.replaceAll(" ^ ", "|^|");
      List tempList = temp.split("|");
      //print("Temp list : $tempList");
      tempList.forEach((element) {
        tokenized.add(element);
      });
    } else {
      tokenized.add(temp);
    }
  }
  ;
  //print("Tokenized $tokenized");
  return tokenized;
}

List tokenizingNegative(List lst) {
  List tokenized = [];
  List tempList = [];
  //print("lst : $lst");
  for (int h = 0; h < lst.length; h++) {
    String temp = lst[h];
    //print("Current string : $temp");
    if (temp.contains('- ')) {
      temp = temp.replaceAll("- ", "-|");
      List tempList = temp.split("|");
      //print("Temp list : $tempList");
      tempList.forEach((element) {
        tokenized.add(element);
      });
    } else {
      tokenized.add(temp);
    }
  }
  ;
  //print("Tokenized $tokenized");
  return tokenized;
}

List tokenizingWhiteSpaceNegative(List lst) {
  List tokenized = [];

  //print("lst : $lst");
  for (int h = 0; h < lst.length; h++) {
    String temp = lst[h];
    //print("Current string : $temp");
    if (temp.contains('- ')) {
      List tempList = [];
      temp = temp.replaceAll("- ", "-");
      tempList.add(temp);
      //print("Temp list : $tempList");
      tempList.forEach((element) {
        //print("Element : $element");
        tokenized.add(element);
      });
    } else {
      //print("Temp : $temp");
      tokenized.add(temp);
    }
  }
  ;
  //print("Tokenized $tokenized");
  return tokenized;
}

double evaluatePostfix(List postfix) {
  postfix = tokenizingWhiteSpaceNegative(postfix);
  //print("Your posfix 2 $postfix");
  double result = 0;
  List values = [];
  double left, right;

  postfix.forEach((i) {
    if (isInteger(i) == true) {
      double temp = double.parse(i);
      values.add(temp);
    } else {
      right = values.last;
      //print("Before remove 1 $values");
      //print("Right $right");
      values.removeLast();
      //print("After remove 1 $values");
      left = values.last;
      //print("Left $left");
      //print("Before remove 2 $values");
      values.removeLast();
      //print("After remove 2 $values");
      result = calculator(left, right, i);
      values.add(result);
      //print("After add result $values");
    }
  });
  //print(values);
  return values.first;
}

double calculator(double x, double y, String operator) {
  double left = x.toDouble();
  double right = y.toDouble();
  double result = 0;
  switch (operator) {
    case "+":
      {
        result = add(left, right);
      }
      break;
    case "-":
      {
        result = substract(left, right);
      }
      break;
    case "*":
      {
        result = mutiply(left, right);
      }
      break;
    case "/":
      {
        result = divide(left, right);
      }
      break;
    case "%":
      {
        result = modulo(left, right);
      }
      break;
    case "^":
      {
        result = exponent(left, right);
      }
      break;
    default:
      {}
      break;
  }

  return result;
}

double add(double x, double y) {
  double left = x.toDouble();
  double right = y.toDouble();
  double result = left + right;
  //print("Result : $result");
  return result;
}

double substract(double x, double y) {
  double left = x.toDouble();
  double right = y.toDouble();
  double result = left - right;
  //print("Result : $result");
  return result;
}

double mutiply(double x, double y) {
  double left = x.toDouble();
  double right = y.toDouble();
  double result = left * right;
  //print("Result : $result");
  return result;
}

double divide(double x, double y) {
  double left = x.toDouble();
  double right = y.toDouble();
  double result = left / right;
  //print("Result : $result");
  return result;
}

double modulo(double x, double y) {
  double left = x.toDouble();
  double right = y.toDouble();
  double result = left % right;
  //print("Result : $result");
  return result;
}

double exponent(double x, double y) {
  double left = x.toDouble();
  double right = y.toDouble();
  double expo = left;
  double result = 1;
  for (int i = 0; i < right; i++) {
    result = result * expo;
  }
  //print("Result : $result");
  return result;
}

void main(List<String> arguments) {
  print('Input your infix expression:');
  String exprs = stdin.readLineSync() as String;
  List tokenizingExprs = tokenizing(exprs);
  List Postfix = toPostFix(tokenizingExprs);
  print("Your postfix: $Postfix");
  double result = evaluatePostfix(Postfix);
  print("Your result after evaluate postfix: $result");
}
